import Vue from "vue";
import VueRouter from "vue-router";
import Rules from "../views/rules/index.vue";
import Rule from "../views/rule/index.vue";
import Presets from "../views/presets/index.vue";
import Preset from "../views/preset/index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/rules",
    name: "rules",
    component: Rules
  },
  {
    path: "/rules/watch/:id",
    name: "ruleWatch",
    component: Rule,
    props: { readOnly: true }
  },
  {
    path: "/rules/edit/:id",
    name: "ruleEdit",
    component: Rule,
    props: { readOnly: false }
  },
  {
    path: "/presets",
    name: "presets",
    component: Presets
  },
  {
    path: "/presets/watch/:id",
    name: "presetWatch",
    component: Preset,
    props: { readOnly: true }
  },
  {
    path: "/presets/edit/:id",
    name: "presetEdit",
    component: Preset,
    props: { readOnly: false }
  }
];

const router = new VueRouter({
  mode: "history",
  base: "/polygon/rules/page", //process.env.BASE_URL
  routes
});

export default router;
