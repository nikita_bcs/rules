const rules = {
  state: {
    rules: [
      {
        id: 1,
        title: "Распределение задач",
        trigger: "OOS",
        solution: "Task",
        source: "OSA",
        description: "",
        checked: true,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 1,
            x: -350,
            y: -70,
            type: "event",
            label: "OOSh"
          },
          {
            id: 2,
            x: -390,
            y: 80,
            type: "actionRequest",
            label: "Определить категорию"
          },
          {
            id: 3,
            x: -350,
            y: 220,
            type: "condition",
            label: "and"
          },
          {
            id: 4,
            x: -550,
            y: 350,
            type: "actionRequest",
            label: "Расчет трудозатрат на выполнение задачи"
          },
          {
            id: 5,
            x: -200,
            y: 350,
            type: "actionRequest",
            label:
              "Расчет свободных ресурсов сотрудника, отвечающего за категорию"
          },
          {
            id: 6,
            x: -350,
            y: 480,
            type: "condition",
            label: "and"
          },
          {
            id: 7,
            x: -390,
            y: 630,
            type: "actionRequest",
            label: "Анализ расчетов"
          },
          {
            id: 8,
            x: -350,
            y: 750,
            type: "condition",
            label: "or"
          },
          {
            id: 9,
            x: -500,
            y: 880,
            type: "event",
            label: "Нет ресурсов"
          },
          {
            id: 10,
            x: -200,
            y: 880,
            type: "event",
            label: "Есть ресурсы"
          },
          {
            id: 11,
            x: -240,
            y: 990,
            type: "actionRequest",
            label: "Оповещение сотруднику, ответственному за категорию"
          },
          {
            id: 12,
            x: -240,
            y: 1100,
            type: "actionAction",
            label: "Сотрудник выполняет задачу"
          },
          {
            id: 13,
            x: -200,
            y: 1220,
            type: "event",
            label: "Результат выполнения задачи"
          }
        ],
        links: [
          {
            id: 3,
            from: 1, // node id the link start
            to: 2 // node id the link end
          },
          {
            id: 4,
            from: 2,
            to: 3
          },
          {
            id: 5,
            from: 3, // node id the link start
            to: 4 // node id the link end
          },
          {
            id: 6,
            from: 3, // node id the link start
            to: 5 // node id the link end
          },
          {
            id: 7,
            from: 4, // node id the link start
            to: 6 // node id the link end
          },
          {
            id: 8,
            from: 5, // node id the link start
            to: 6 // node id the link end
          },
          {
            id: 9,
            from: 6, // node id the link start
            to: 7 // node id the link end
          },
          {
            id: 10,
            from: 7, // node id the link start
            to: 8 // node id the link end
          },
          {
            id: 11,
            from: 8, // node id the link start
            to: 9 // node id the link end
          },
          {
            id: 12,
            from: 8, // node id the link start
            to: 10 // node id the link end
          },
          {
            id: 13,
            from: 10, // node id the link start
            to: 11 // node id the link end
          },
          {
            id: 14,
            from: 11, // node id the link start
            to: 12 // node id the link end
          },
          {
            id: 15,
            from: 12, // node id the link start
            to: 13 // node id the link end
          }
        ]
      },
      {
        id: 2,
        title: "Фрод мониторинг",
        trigger: "OOS",
        solution: "Task",
        source: "DA",
        description: "",
        checked: false,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 1,
            x: -700,
            y: -69,
            type: "event",
            label: "OOS"
          },
          {
            id: 2,
            x: -357,
            y: 80,
            type: "actionRequest",
            label: "Определить категорию"
          },
          {
            id: 3,
            x: -257,
            y: 220,
            type: "actionAction",
            label: "Выполнить задачу"
          }
        ],
        links: [
          {
            id: 3,
            from: 1, // node id the link start
            to: 2 // node id the link end
          },
          {
            id: 4,
            from: 2,
            to: 3
          }
        ]
      },
      {
        id: 3,
        title: "Получение инцедентов",
        trigger: "OOS",
        solution: "Task",
        source: "Pulsar",
        description: "",
        checked: true,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 1,
            x: -700,
            y: -69,
            type: "actionRequest",
            label: "Направление запроса на получение инцидентов в Pulsar"
          },
          {
            id: 2,
            x: -700,
            y: 80,
            type: "actionRequest",
            label: "Получает инциденты"
          },
          {
            id: 3,
            x: -660,
            y: 220,
            type: "event",
            label: "Правило №1"
          }
        ],
        links: [
          {
            id: 3,
            from: 1, // node id the link start
            to: 2 // node id the link end
          },
          {
            id: 4,
            from: 2,
            to: 3
          }
        ]
      }
    ],
    attrState: null
  },
  getters: {
    getRule: state => id => {
      const rule = state.rules.find(rule => rule.id === parseInt(id, 10));
      return rule;
    }
  },
  mutations: {},
  action: {}
};
export default rules;
