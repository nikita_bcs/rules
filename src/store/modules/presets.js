const presets = {
  state: {
    presets: [
      {
        id: 1,
        title: "Событие OOS",
        trigger: "OOS",
        solution: "Task",
        source: "OSA",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusamus debitis facere nihil eum nesciunt placeat! Voluptate, ipsa itaque.",
        checked: false,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 2,
            x: -700,
            y: -69,
            type: "Action",
            label: "test1"
          },
          {
            id: 4,
            x: -357,
            y: 80,
            type: "Script",
            label: "test2"
          },
          {
            id: 6,
            x: -557,
            y: 80,
            type: "preset",
            label: "test3"
          }
        ],
        links: [
          {
            id: 3,
            from: 2, // node id the link start
            to: 4 // node id the link end
          }
        ]
      },
      {
        id: 2,
        title: "Событие OOSh",
        trigger: "OOS",
        solution: "Task",
        source: "OSA",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusamus debitis facere nihil eum nesciunt placeat! Voluptate, ipsa itaque.",
        checked: false,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 2,
            x: -700,
            y: -69,
            type: "Action",
            label: "test1"
          },
          {
            id: 4,
            x: -357,
            y: 80,
            type: "Script",
            label: "test2"
          },
          {
            id: 6,
            x: -557,
            y: 80,
            type: "preset",
            label: "test3"
          }
        ],
        links: [
          {
            id: 3,
            from: 2, // node id the link start
            to: 4 // node id the link end
          }
        ]
      },
      {
        id: 3,
        title: "Опаздание сотрудника",
        trigger: "OOS",
        solution: "Task",
        source: "DA",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusamus debitis facere nihil eum nesciunt placeat! Voluptate, ipsa itaque.",
        checked: true,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 2,
            x: -700,
            y: -69,
            type: "Action",
            label: "test1"
          },
          {
            id: 4,
            x: -357,
            y: 80,
            type: "Script",
            label: "test2"
          },
          {
            id: 6,
            x: -557,
            y: 80,
            type: "preset",
            label: "test3"
          }
        ],
        links: [
          {
            id: 3,
            from: 2, // node id the link start
            to: 4 // node id the link end
          }
        ]
      },
      {
        id: 4,
        title: "Жалоба покупателя",
        trigger: "OOS",
        solution: "Task",
        source: "Клиентское приложение || Горячая линия",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusamus debitis facere nihil eum nesciunt placeat! Voluptate, ipsa itaque.",
        checked: true,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 2,
            x: -700,
            y: -69,
            type: "Action",
            label: "test1"
          },
          {
            id: 4,
            x: -357,
            y: 80,
            type: "Script",
            label: "test2"
          },
          {
            id: 6,
            x: -557,
            y: 80,
            type: "preset",
            label: "test3"
          }
        ],
        links: [
          {
            id: 3,
            from: 2, // node id the link start
            to: 4 // node id the link end
          }
        ]
      },
      {
        id: 5,
        title: "Поставка",
        trigger: "OOS",
        solution: "Task",
        source: "Расписание",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusamus debitis facere nihil eum nesciunt placeat! Voluptate, ipsa itaque.",
        checked: false,
        centerX: 1024,
        centerY: 140,
        scale: 1,
        nodes: [
          {
            id: 2,
            x: -700,
            y: -69,
            type: "Action",
            label: "test1"
          },
          {
            id: 4,
            x: -357,
            y: 80,
            type: "Script",
            label: "test2"
          },
          {
            id: 6,
            x: -557,
            y: 80,
            type: "preset",
            label: "test3"
          }
        ],
        links: [
          {
            id: 3,
            from: 2, // node id the link start
            to: 4 // node id the link end
          }
        ]
      }
    ]
  },
  getters: {
    getpreset: state => id => {
      const preset = state.presets.find(
        preset => preset.id === parseInt(id, 10)
      );
      return preset;
    }
  },
  mutations: {},
  action: {}
};
export default presets;
