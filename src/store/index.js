import Vue from "vue";
import Vuex from "vuex";
import presets from "./modules/presets.js";
import rules from "./modules/rules.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    presets,
    rules
  }
});
